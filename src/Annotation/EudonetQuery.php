<?php

namespace Drupal\eudonet\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Eudonet query item annotation object.
 *
 * @see \Drupal\eudonet\Plugin\EudonetQueryManager
 * @see plugin_api
 *
 * @Annotation
 */
class EudonetQuery extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The query resource base path.
   *
   * @var string
   */
  public $path;

  /**
   * The default request method.
   *
   * @var string
   */
  public $method;

  /**
   * Determine if the request requires authentication.
   *
   * @var bool
   */
  public $authentication;

  /**
   * The EudonetQueryResult plugin. Default to 'eudonet_default_query_result'.
   *
   * @var string
   */
  public $query_result = 'eudonet_default_query_result';

}
