<?php

namespace Drupal\eudonet\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Eudonet query result item annotation object.
 *
 * @see \Drupal\eudonet\Plugin\EudonetQueryResultManager
 * @see plugin_api
 *
 * @Annotation
 */
class EudonetQueryResult extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
