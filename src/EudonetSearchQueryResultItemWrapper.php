<?php

namespace Drupal\eudonet;

use Drupal\eudonet\Traits\EudonetMappingTrait;

/**
 * Class EudonetQueryResultItemWrapper.
 *
 * @package Drupal\eudonet
 */
class EudonetSearchQueryResultItemWrapper {

  use EudonetMappingTrait;

  private $item;

  /**
   * EudonetQueryResultItemWrapper constructor.
   *
   * @param array $item
   *   The item object from response.
   * @param string $mapping
   *   The mapping plugin id.
   */
  public function __construct(array $item, $mapping = 'default') {
    $this->item = $item;
    $this->setMapping($mapping);
  }

  /**
   * Get the item id.
   *
   * @return int
   *   The file id for this item.
   */
  public function id() {
    return $this->item['FileId'];
  }

  /**
   * Use magic getter to wrap the array item.
   *
   * @param string $name
   *   The desc id or a valid mapped key.
   *
   * @return mixed|null
   *   An array representing the value according to the $name parameter.
   */
  public function __get($name) {
    $desc_id = $this->mapping[$name] ?? $name;
    $path = [];
    $path = self::getDescIdPath($desc_id, $this->item, $path);
    if (!empty($path)) {
      return self::getDescIdItem($this->item, $path);
    }
    return NULL;
  }

  /**
   * Recursively get the desc_id parent path.
   *
   * @param string $desc_id
   *   The desc id to look for.
   * @param array $tree
   *   A tree representing an eudonet query result item.
   * @param array $path
   *   The path to the desc id.
   *
   * @return array|null
   *   The desc id parent path or NULL.
   */
  private static function getDescIdPath($desc_id, $tree, $path) {
    $found = NULL;
    foreach ($tree as $key => $item) {
      if ($key == 'DescId' && $item == $desc_id) {
        return $path;
      }
      elseif (is_array($item) && $found === NULL) {
        $found = self::getDescIdPath($desc_id, $item, array_merge($path, [$key]));
      }
    }
    return $found;
  }

  /**
   * Get the item value according to a desc id.
   *
   * @param array $tree
   *   A tree representing an eudonet query result item.
   * @param array $path
   *   The path to the desc id.
   * @param int $depth
   *   The tree depth.
   *
   * @return mixed|null
   *   Return the value according to the desc_id path or NULL;
   */
  private static function getDescIdItem($tree, $path, $depth = 0) {
    $max_depth = count($path);
    $found = NULL;
    foreach ($tree as $key => $item) {
      if ($depth == $max_depth - 1 && isset($path[$depth]) && $key == $path[$depth]) {
        return (object) $item;
      }
      elseif (is_array($item) && $depth < $max_depth && $found === NULL) {
        $found = self::getDescIdItem($item, $path, $depth + 1);
      }
    }
    return $found;
  }

}
