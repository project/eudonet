<?php

namespace Drupal\eudonet;

use Drupal\eudonet\Traits\EudonetMappingTrait;
use Drupal\eudonet\Traits\EudonetQueryConditionTrait;

/**
 * Class EudonetQueryCondition.
 *
 * @package Drupal\eudonet
 */
class EudonetQueryCondition {

  use EudonetQueryConditionTrait;
  use EudonetMappingTrait;

  /**
   * The field name or field id.
   *
   * @var int|string
   */
  private $field;

  /**
   * The field value.
   *
   * @var mixed
   */
  private $value;

  /**
   * The field operator.
   *
   * @var string
   */
  private $operator;

  /**
   * The fields operator.
   *
   * @var string
   */
  private $interOperator;

  /**
   * EudonetQueryCondition constructor.
   *
   * @param string|int $field
   *   The field name or field id.
   * @param mixed $value
   *   The field value used to filter the query.
   * @param string $operator
   *   The operator use with the $value.
   * @param string $inter_operator
   *   The inter operator used in the group case.
   * @param string $mapping
   *   The mapping plugin id to use (default value => 'default').
   */
  public function __construct($field, $value, $operator, $inter_operator = 'None', $mapping = 'default') {
    $this->field = $field;
    $this->value = $value;
    $this->operator = $operator;
    $this->interOperator = $inter_operator;
    $this->setMapping($mapping);
  }

  /**
   * Determine if the current object is a group or a condition criteria.
   *
   * @return bool
   *   TRUE if the current object is a group condition, FALSE otherwise.
   */
  public function isGroup() {
    return $this->interOperator != 'None';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      'WhereCustoms' => [],
      'Criteria' => NULL,
      'InterOperator' => Eudonet::INTER_OPERATORS[$this->interOperator],
    ];
    if (!empty($this->field) && !empty($this->operator)) {
      $field_name = $this->field;
      if (!empty($this->mapping[$this->field])) {
        $field_name = $this->mapping[$this->field];
      }
      $build['Criteria'] = [
        'Operator' => Eudonet::OPERATORS[$this->operator],
        'Field' => $field_name,
        'Value' => $this->value,
      ];
    }
    if ($this->isGroup() && !empty($this->conditions)) {
      $build['InterOperator'] = Eudonet::INTER_OPERATORS['AND'];
      foreach ($this->conditions as $condition) {
        $build['WhereCustoms'][] = $condition->build();
      }
    }
    return $build;
  }

}
