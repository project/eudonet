<?php

namespace Drupal\eudonet\Traits;

/**
 * Trait EudonetQueryPagerTrait.
 *
 * @package Drupal\eudonet\Traits
 */
trait EudonetQueryPagerTrait {

  protected $pager = [];

  /**
   * Define the pager for the request.
   *
   * @param int $length
   *   The number of items per page (default to 0).
   * @param int $offset
   *   The page index (default to 0 = first page).
   */
  public function pager($length = 0, $offset = 0) {
    $this->pager = [
      'RowsPerPage' => $length,
      'NumPage' => $offset,
    ];
  }

}
