<?php

namespace Drupal\eudonet\Traits;

/**
 * Trait EudonetQueryResultPagerTrait.
 *
 * This trait should be used in a query result object when multiple pages are
 * needed.
 *
 * @package Drupal\eudonet\Traits
 */
trait EudonetQueryResultPagerTrait {

  /**
   * Get the current page index.
   *
   * @return int
   *   The current page index.
   */
  public function getCurrentPage() {
    return $this->response['ResultMetaData']['NumPage'];
  }

  /**
   * Get the number of pages.
   *
   * @return int
   *   The total pages for this request.
   */
  public function getTotalPages() {
    return $this->response['ResultMetaData']['TotalPages'];
  }

  /**
   * Get the number of rows.
   *
   * @return int
   *   The total rows for this request.
   */
  public function getTotalRows() {
    return $this->response['ResultMetaData']['TotalRows'];
  }

  /**
   * Get the next page index.
   *
   * @return int
   *   The next page index.
   */
  public function getNextPage() {
    return $this->getCurrentPage() + 1;
  }

  /**
   * Test if the current request has a next page to get.
   *
   * @return bool
   *   TRUE if a next page should be taken into account, FALSE otherwise.
   */
  public function hasNextPage() {
    return $this->getNextPage() <= $this->getTotalPages();
  }

}
