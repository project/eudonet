<?php

namespace Drupal\eudonet\Traits;

/**
 * Trait EudonetQueryOrderByTrait.
 *
 * @package Drupal\eudonet\Traits
 */
trait EudonetQueryOrderByTrait {

  protected $orderBy = [
    'OrderBy' => [],
  ];

  /**
   * Order the query by a field name.
   *
   * @param int|string $field_name
   *   The field name to get in the request response.
   * @param int $order
   *   The order as int.
   */
  public function orderBy($field_name, $order = 0) {
    $field = $field_name;
    if (method_exists($this, 'ensureMapping')) {
      $this->ensureMapping();
    }
    if (isset($this->mapping) && !empty($this->mapping[$field_name])) {
      $field = $this->mapping[$field_name];
    }
    $this->orderBy['OrderBy'][] = [
      'DescId' => $field,
      'Order' => $order,
    ];
  }

}
