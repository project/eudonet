<?php

namespace Drupal\eudonet\Traits;

/**
 * Trait EudonetMappingTrait.
 *
 * @package Drupal\eudonet\Traits
 */
trait EudonetMappingTrait {

  /**
   * The current mapping plugin id.
   *
   * @var string
   */
  protected $mappingId;

  /**
   * The current mapping definition.
   *
   * @var array
   */
  protected $mapping;

  /**
   * Set the mapping for a query or a query result.
   *
   * @param string $mapping_id
   *   The mapping plugin id.
   */
  public function setMapping($mapping_id) {
    $this->mappingId = $mapping_id;
    $this->ensureMapping();
  }

  /**
   * Make sure to get the plugin definition.
   */
  protected function ensureMapping() {
    if (isset($this->eudonetMapping)) {
      $this->mapping = $this->eudonetMapping->getDefinition($this->mappingId);
    }
    else {
      $this->mapping = \Drupal::service('plugin.manager.eudonet_mapping')
        ->getDefinition($this->mappingId);
    }
  }

}
