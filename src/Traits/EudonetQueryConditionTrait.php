<?php

namespace Drupal\eudonet\Traits;

use Drupal\eudonet\EudonetQueryCondition;

/**
 * Trait EudonetQueryConditionTrait.
 *
 * @package Drupal\eudonet\Traits
 */
trait EudonetQueryConditionTrait {

  /**
   * An array containing all conditions.
   *
   * @var \Drupal\eudonet\EudonetQueryCondition[]
   */
  public $conditions = [];

  /**
   * Filter the query with a condition.
   *
   * @param string $field
   *   The field name (if mapping provided) or the fieldId.
   * @param string $value
   *   The field value.
   * @param string $operator
   *   The operator used to filter the field.
   */
  public function condition($field, $value, $operator = '=') {
    $iop = 'None';
    if (count($this->conditions) > 0) {
      $iop = (!empty($this->interOperator)) ? $this->interOperator : 'AND';
    }
    $this->conditions[] = new EudonetQueryCondition($field, $value, $operator, $iop, $this->mappingId);
  }

  /**
   * Add a group condition.
   *
   * @param \Drupal\eudonet\EudonetQueryCondition $condition
   *   A condition object representing the condition group.
   */
  public function conditionGroup(EudonetQueryCondition $condition) {
    $this->conditions[] = $condition;
  }

  /**
   * Create a 'OR' condition group.
   *
   * @see EudonetQueryConditionTrait::group()
   *
   * @return EudonetQueryCondition
   *   A condition object ready to use to make 'OR' conditions.
   */
  public function orGroup() {
    return $this->group('OR');
  }

  /**
   * Create a 'AND' condition group.
   *
   * @see EudonetQueryConditionTrait::group()
   *
   * @return EudonetQueryCondition
   *   A condition object ready to use to make 'AND' conditions.
   */
  public function andGroup() {
    return $this->group('AND');
  }

  /**
   * Create a condition group according to the Eudonet API inter operators.
   *
   * @param string $inter_operator
   *   The inter operator, use EudonetHelper::INTER_OPERATORS const for
   *   convenience.
   *
   * @return EudonetQueryCondition
   *   A condition object to add conditions and append it to the query plugin.
   */
  public function group($inter_operator) {
    return new EudonetQueryCondition(NULL, NULL, NULL, $inter_operator, $this->mappingId);
  }

}
