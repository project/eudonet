<?php

namespace Drupal\eudonet\Traits;

/**
 * Trait EudonetQueryFieldTrait.
 *
 * @package Drupal\eudonet\Traits
 */
trait EudonetQueryFieldTrait {

  protected $fields = [];

  /**
   * Add a field.
   *
   * @param int|string $field_name
   *   The field name to get in the request response.
   */
  public function addField($field_name) {
    $field = $field_name;
    if (method_exists($this, 'ensureMapping')) {
      $this->ensureMapping();
    }
    if (isset($this->mapping) && !empty($this->mapping[$field_name])) {
      $field = $this->mapping[$field_name];
    }
    $this->fields[] = $field;
  }

  /**
   * Add multiple fields.
   *
   * @param string[] $field_names
   *   An array of field names to get in the request response.
   */
  public function addFields($field_names) {
    foreach ($field_names as $field_name) {
      $this->addField($field_name);
    }
  }

}
