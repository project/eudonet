<?php

namespace Drupal\eudonet\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Traits\EudonetMappingTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Eudonet query plugins.
 */
abstract class EudonetQueryBase extends PluginBase implements EudonetQueryInterface, ContainerFactoryPluginInterface {

  use MessengerTrait;
  use StringTranslationTrait;
  use EudonetMappingTrait;

  /**
   * The http client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * The query result manager.
   *
   * @var \Drupal\eudonet\Plugin\EudonetQueryResultManager
   */
  protected $eudonetQueryResultManager;

  /**
   * The Eudonet helper service.
   *
   * @var \Drupal\eudonet\Eudonet
   */
  protected $eudonet;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $http_client_factory, Eudonet $eudonet, EudonetQueryResultManager $eudonet_query_result_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClientFactory = $http_client_factory;
    $this->eudonet = $eudonet;
    $this->eudonetQueryResultManager = $eudonet_query_result_manager;
    $this->mappingId = 'default';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('http_client_factory'),
      $container->get('eudonet'),
      $container->get('plugin.manager.eudonet_query_result')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->pluginDefinition['path'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->pluginDefinition['method'];
  }

  /**
   * {@inheritdoc}
   */
  public function needAuthentication() {
    return $this->pluginDefinition['authentication'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function execute() {
    $global_config = $this->eudonet->getGlobalSettings();

    $options = [];
    if ($this->getMethod() == 'POST') {
      $options['json'] = $this->build();
      $options['timeout'] = 0;
    }

    $config = [
      'base_uri' => $global_config->get('base_url'),
    ];
    if ($this->needAuthentication()) {
      $token_info = $global_config->get('token_info');
      $token = $token_info['token'];
      $expiration = $token_info['expiration'] ?? '';
      if (empty($expiration) || strtotime($expiration) < strtotime('+2 hours')) {
        /** @var \Drupal\eudonet\Plugin\EudonetQueryResult\AuthenticationQueryResult $auth_result */
        $auth_result = $this->eudonet->authenticate();
        if ($auth_result->success()) {
          $token = $auth_result->getToken();
        }
      }
      if (empty($token)) {
        $error = $this->t('Authentication token not found.');
        $this->messenger()->addError($error);
        \Drupal::logger('eudonet')->error($error);
      }
      $config['headers']['x-auth'] = $token;
    }
    $client = $this->httpClientFactory->fromOptions($config);
    $response = $client->request($this->getMethod(), $this->getPath(), $options);
    return $this->eudonetQueryResultManager->createInstance($this->pluginDefinition['query_result'], [
      'response' => $response,
      'mapping' => $this->mappingId,
    ]);
  }

}
