<?php

namespace Drupal\eudonet\Plugin\EudonetQueryResult;

use Countable;
use Drupal\eudonet\EudonetSearchQueryResultItemWrapper;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryResultBase;
use Drupal\eudonet\Traits\EudonetQueryResultPagerTrait;
use Iterator;

/**
 * Class SearchQueryResult.
 *
 * @package Drupal\eudonet\Plugin\EudonetQueryResult
 *
 * @EudonetQueryResult(
 *   id = "eudonet_search_query_result",
 *   label = @Translation("Search query result")
 * )
 */
class SearchQueryResult extends EudonetQueryResultBase implements Iterator, Countable {

  use EudonetQueryResultPagerTrait;

  /**
   * The current table index.
   *
   * @var int
   */
  private $position;

  /**
   * The tables.
   *
   * @var EudonetSearchQueryResultItemWrapper[]
   */
  private $items;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, Eudonet $eudonet) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $eudonet);
    $this->items = $this->response['ResultData']['Rows'] ?? [];
    foreach ($this->items as &$item) {
      $item = new EudonetSearchQueryResultItemWrapper($item, $configuration['mapping'] ?? 'default');
    }
  }

  /**
   * Get the table name of the search.
   */
  public function label() {
    return $this->response['ResultMetaData']['Tables'][0]['Label'] ?? '';
  }

  /**
   * Get items as array.
   *
   * @return array
   *   An array of items from the Eudonet API.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function current() {
    return $this->items[$this->position];
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function next() {
    ++$this->position;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function key() {
    return $this->position;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function valid() {
    return isset($this->items[$this->position]);
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function rewind() {
    $this->position = 0;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function count() {
    return count($this->items);
  }

}
