<?php

namespace Drupal\eudonet\Plugin\EudonetQueryResult;

use Countable;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryResultBase;
use Iterator;

/**
 * Class MetaInfosQueryResult.
 *
 * @package Drupal\eudonet\Plugin\EudonetQueryResult
 *
 * @EudonetQueryResult(
 *   id = "eudonet_meta_infos_query_result",
 *   label = @Translation("Meta infos query result")
 * )
 */
class MetaInfosQueryResult extends EudonetQueryResultBase implements Iterator, Countable {

  /**
   * The current table index.
   *
   * @var int
   */
  private $position;

  /**
   * The tables.
   *
   * @var array
   */
  private $items;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, Eudonet $eudonet) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $eudonet);
    $this->items = $this->response['ResultMetaData']['Tables'] ?? [];
  }

  /**
   * Get tables as array.
   *
   * @return array
   *   An array of array representing tables from the Eudonet API.
   */
  public function getTables() {
    return $this->items;
  }

  /**
   * {@inheritdoc}
   */
  public function current() {
    return $this->items[$this->position];
  }

  /**
   * {@inheritdoc}
   */
  public function next() {
    ++$this->position;
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    return $this->position;
  }

  /**
   * {@inheritdoc}
   */
  public function valid() {
    return isset($this->items[$this->position]);
  }

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    $this->position = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return count($this->items);
  }

}
