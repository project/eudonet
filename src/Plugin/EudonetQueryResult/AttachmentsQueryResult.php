<?php

namespace Drupal\eudonet\Plugin\EudonetQueryResult;

use Drupal\eudonet\Plugin\EudonetQueryResultBase;

/**
 * Class AttachmentsQueryResult.
 *
 * @package Drupal\eudonet\Plugin\EudonetQueryResult
 *
 * @EudonetQueryResult(
 *   id = "eudonet_attachments_query_result",
 *   label = @Translation("Attachments query result")
 * )
 */
class AttachmentsQueryResult extends EudonetQueryResultBase {

  /**
   * Get the server url according to the attachment upload.
   *
   * @return string
   *   The server url.
   */
  public function getServerUrl() {
    return $this->response['ResultData']['ServerUrl'];
  }

}
