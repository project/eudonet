<?php

namespace Drupal\eudonet\Plugin\EudonetQueryResult;

use Drupal\eudonet\Plugin\EudonetQueryResultBase;

/**
 * Class DefaultQueryResult.
 *
 * @package Drupal\eudonet\Plugin\EudonetQueryResult
 *
 * @EudonetQueryResult(
 *   id = "eudonet_default_query_result",
 *   label = @Translation("Eudonet default query result")
 * )
 */
class DefaultQueryResult extends EudonetQueryResultBase {

}
