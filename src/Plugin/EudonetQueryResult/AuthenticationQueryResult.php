<?php

namespace Drupal\eudonet\Plugin\EudonetQueryResult;

use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryResultBase;

/**
 * Class AuthenticationQueryResult.
 *
 * @package Drupal\eudonet\Plugin\EudonetQueryResult
 *
 * @EudonetQueryResult(
 *   id = "eudonet_authentication_query_result",
 *   label = @Translation("Authentication query result")
 * )
 */
class AuthenticationQueryResult extends EudonetQueryResultBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, Eudonet $eudonet) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $eudonet);
    $settings = $this->eudonet->getEditableGlobalSettings();
    $settings->set('token_info', [
      'expiration' => $this->response['ResultData']['ExpirationDate'],
      'server_date' => $this->response['ResultData']['ServerDate'],
      'token' => $this->response['ResultData']['Token'],
    ])->save();
  }

  /**
   * Get the token from the Eudonet API response.
   *
   * @return string
   *   A string representing the authentication token.
   */
  public function getToken() {
    return $this->response['ResultData']['Token'];
  }

}
