<?php

namespace Drupal\eudonet\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Traits\EudonetMappingTrait;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Eudonet query result plugins.
 */
abstract class EudonetQueryResultBase extends PluginBase implements EudonetQueryResultInterface, ContainerFactoryPluginInterface {

  use EudonetMappingTrait;

  /**
   * The guzzle response object.
   *
   * @var \GuzzleHttp\Psr7\Response
   */
  protected $guzzleResponse;

  /**
   * The response array from the Eudonet API.
   *
   * @var array
   */
  protected $response;

  /**
   * The guzzle response header.
   *
   * @var array
   */
  protected $headers;

  /**
   * Mechanize to ensure the authentication when needed.
   *
   * @var bool
   */
  protected $firstTry;

  /**
   * The Eudonet Helper service.
   *
   * @var \Drupal\eudonet\Eudonet
   */
  protected $eudonet;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, Eudonet $eudonet) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eudonet = $eudonet;
    $this->guzzleResponse = $this->configuration['response'];
    $this->response = Json::decode($this->guzzleResponse->getBody()
      ->getContents());
    $this->headers = $this->guzzleResponse->getHeaders();
    $this->firstTry = TRUE;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('eudonet')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function success() {
    return $this->response['ResultInfos']['Success'];
  }

  /**
   * {@inheritdoc}
   */
  public function ensureAuth() {
    if ($this->firstTry && in_array($this->getErrorNumber(), Eudonet::TOKEN_ERRORS)) {
      $this->firstTry = FALSE;
      try {
        return $this->eudonet->authenticate();
      }
      catch (PluginException $e) {
        \Drupal::logger('eudonet')->error($e->getMessage());
      }
      catch (GuzzleException $e) {
        \Drupal::logger('eudonet')->warning($e->getMessage());
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiMessage() {
    return $this->response['ResultInfos']['ApiMessage'];
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorMessage() {
    return $this->response['ResultInfos']['ErrorMessage'];
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorNumber() {
    return $this->response['ResultInfos']['ErrorNumber'];
  }

  /**
   * {@inheritdoc}
   */
  public function getGuzzleResponse() {
    return $this->guzzleResponse;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxAllowedRequests() {
    return intval($this->headers['X-CALL-MAX'][0]);
  }

  /**
   * {@inheritdoc}
   */
  public function getRemainingCallsByIp() {
    return intval($this->headers['X-CALL-BYIP-REMAIN'][0]);
  }

  /**
   * {@inheritdoc}
   */
  public function getRemainingCalls() {
    return intval($this->headers['X-CALL-REMAIN'][0]);
  }

  /**
   * {@inheritdoc}
   */
  public function isExceedQuotas() {
    return $this->getRemainingCalls() == 0 && $this->getRemainingCallsByIp() == 0;
  }

}
