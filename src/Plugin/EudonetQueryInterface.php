<?php

namespace Drupal\eudonet\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Eudonet query plugins.
 */
interface EudonetQueryInterface extends PluginInspectionInterface {

  /**
   * Build the API path.
   *
   * @return string
   *   An API valid resource.
   */
  public function getPath();

  /**
   * Get the request method.
   *
   * @return string
   *   Return the method used by the query.
   */
  public function getMethod();

  /**
   * Determine if the request requires authentication.
   *
   * @return bool
   *   TRUE if the request requires authentication, FALSE otherwise.
   */
  public function needAuthentication();

  /**
   * Build the request content.
   *
   * @return array
   *   Return an array representing the content of the request.
   */
  public function build();

  /**
   * Make the request to the eudonet server.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQueryResultInterface $response
   *   A query result object (wrapper for the guzzle response).
   */
  public function execute();

}
