<?php

namespace Drupal\eudonet\Plugin\EudonetQuery;

use Drupal\Core\Http\ClientFactory;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryBase;
use Drupal\eudonet\Plugin\EudonetQueryResultManager;

/**
 * Class CUDQuery.
 *
 * @package Drupal\eudonet\Plugin\EudonetQuery
 *
 * @EudonetQuery(
 *   id = "eudonet_cud_query",
 *   label = @Translation("Create/Update/Delete"),
 *   path = "CUD/",
 *   method = "POST",
 *   authentication = TRUE,
 * )
 */
class CUDQuery extends EudonetQueryBase {

  protected $fields;

  protected $imageValue;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $http_client_factory, Eudonet $eudonet, EudonetQueryResultManager $eudonet_query_result_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client_factory, $eudonet, $eudonet_query_result_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return (strpos($this->configuration['additional_path'], 'Delete') === 0) ? 'DELETE' : parent::getMethod();
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return parent::getPath() . $this->configuration['additional_path'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    if (!empty($this->fields)) {
      $build['Fields'] = $this->fields;
    }
    elseif (!empty($this->imageValue)) {
      $build = $this->imageValue;
    }
    return $build;
  }

  /**
   * Set a value for a specific field.
   *
   * This method MUST not be used from a Update Image query.
   * Use setImageValue() instead.
   *
   * @param int|string $field_name
   *   The field_name to update.
   * @param mixed $value
   *   The value to set.
   */
  public function setValue($field_name, $value) {
    $this->ensureMapping();
    $this->fields[] = [
      'DescId' => $this->mapping[$field_name] ?? $field_name,
      'Value' => $value,
    ];
  }

  /**
   * Set multiple values(field_name => $value)
   *
   * @param array $fields
   *   An array of values keyed by field name.
   *
   * @see setValue()
   */
  public function setValues($fields) {
    foreach ($fields as $field_name => $value) {
      $this->setValue($field_name, $value);
    }
  }

  /**
   * Set an image value for a specific field.
   *
   * @param string $field_name
   *   The field name to update.
   * @param mixed $value
   *   The value to set.
   */
  public function setImageValue($field_name, $value) {
    $this->ensureMapping();
    $this->imageValue = [
      'DescId' => $this->mapping[$field_name] ?? $field_name,
      'Value' => $value,
    ];
  }

}
