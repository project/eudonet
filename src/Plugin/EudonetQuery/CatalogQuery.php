<?php

namespace Drupal\eudonet\Plugin\EudonetQuery;

use Drupal\Core\Http\ClientFactory;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryBase;
use Drupal\eudonet\Plugin\EudonetQueryResultManager;
use Exception;

/**
 * Class CatalogQuery.
 *
 * @package Drupal\eudonet\Plugin\EudonetQuery
 *
 * @EudonetQuery(
 *   id = "eudonet_catalog_query",
 *   label = @Translation("Catalog"),
 *   path = "Catalog/",
 *   method = "GET",
 *   authentication = TRUE,
 *   query_result = "eudonet_catalog_query_result"
 * )
 */
class CatalogQuery extends EudonetQueryBase {

  private $additionalPath;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $http_client_factory, Eudonet $eudonet, EudonetQueryResultManager $eudonet_query_result_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client_factory, $eudonet, $eudonet_query_result_manager);
    $this->additionalPath = $this->configuration['additional_path'] ?? FALSE;
    if (!$this->additionalPath) {
      throw new Exception('A descId or \'Users\' are required to use the catalog resource');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return ($this->additionalPath == 'Users') ? 'POST' : parent::getMethod();
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return parent::getPath() . $this->additionalPath;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'ShowMetaData' => TRUE,
      'WhereCustom' => [
        'WhereCustoms' => [],
        'Criteria' => NULL,
        'InterOperator' => Eudonet::INTER_OPERATORS['None'],
      ],
    ];
  }

}
