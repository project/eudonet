<?php

namespace Drupal\eudonet\Plugin\EudonetQuery;

use Drupal\eudonet\Plugin\EudonetQueryBase;

/**
 * Class AuthenticationQuery.
 *
 * @package Drupal\eudonet\Plugin\EudonetQuery
 *
 * @EudonetQuery(
 *   id = "eudonet_authentication_query",
 *   label = @Translation("Authentication"),
 *   path = "Authenticate/Token",
 *   method = "POST",
 *   authentication = FALSE,
 *   query_result = "eudonet_authentication_query_result"
 * )
 */
class AuthenticationQuery extends EudonetQueryBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->configuration['authentication_parameters'];
  }

}
