<?php

namespace Drupal\eudonet\Plugin\EudonetQuery;

use Drupal\Core\Http\ClientFactory;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryBase;
use Drupal\eudonet\Plugin\EudonetQueryResultManager;

/**
 * Class MetaInfosQuery.
 *
 * @package Drupal\eudonet\Plugin\EudonetQuery
 *
 * @EudonetQuery(
 *   id = "eudonet_meta_infos_query",
 *   label = @Translation("Meta infos"),
 *   path = "MetaInfos/",
 *   method = "POST",
 *   authentication = TRUE,
 *   query_result = "eudonet_meta_infos_query_result"
 * )
 */
class MetaInfosQuery extends EudonetQueryBase {

  /**
   * An array representing a list of table id (DescId).
   *
   * @var array
   */
  protected $tables;

  /**
   * Determine if the query should get all tables from (MetaInfos/ListTabs/).
   *
   * @var bool
   */
  protected $getTableList;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $http_client_factory, Eudonet $eudonet, EudonetQueryResultManager $eudonet_query_result_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client_factory, $eudonet, $eudonet_query_result_manager);
    $this->getTableList = $this->configuration['get_table_list'] ?? FALSE;
    $this->tables = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    if ($this->getTableList) {
      return 'GET';
    }
    return parent::getMethod();
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    $path = parent::getPath();
    if ($this->getTableList) {
      $path .= 'ListTabs/';
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'Tables' => array_map(function ($id) {
        return [
          'DescId' => $id,
          'AllFields' => TRUE,
          'Fields' => [0],
        ];
      }, $this->tables),
    ];
  }

}
