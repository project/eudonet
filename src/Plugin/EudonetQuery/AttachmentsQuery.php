<?php

namespace Drupal\eudonet\Plugin\EudonetQuery;

use Drupal\eudonet\Plugin\EudonetQueryBase;

/**
 * Class AttachmentsQuery.
 *
 * @package Drupal\eudonet\Plugin\EudonetQuery
 *
 * @EudonetQuery(
 *   id = "eudonet_attachments_query",
 *   label = @Translation("Attachments"),
 *   path = "Annexes/Add",
 *   method = "POST",
 *   authentication = TRUE
 * )
 */
class AttachmentsQuery extends EudonetQueryBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->configuration['attachment_parameters'];
  }

}
