<?php

namespace Drupal\eudonet\Plugin\EudonetQuery;

use Drupal\Core\Http\ClientFactory;
use Drupal\eudonet\Eudonet;
use Drupal\eudonet\Plugin\EudonetQueryBase;
use Drupal\eudonet\Plugin\EudonetQueryResultManager;
use Drupal\eudonet\Traits\EudonetQueryConditionTrait;
use Drupal\eudonet\Traits\EudonetQueryFieldTrait;
use Drupal\eudonet\Traits\EudonetQueryOrderByTrait;
use Drupal\eudonet\Traits\EudonetQueryPagerTrait;
use Exception;

/**
 * Class SearchQuery.
 *
 * @package Drupal\eudonet\Plugin\EudonetQuery
 *
 * @EudonetQuery(
 *   id = "eudonet_search_query",
 *   label = @Translation("Search"),
 *   path = "Search/",
 *   method = "GET",
 *   authentication = TRUE,
 *   query_result = "eudonet_search_query_result"
 * )
 */
class SearchQuery extends EudonetQueryBase {

  use EudonetQueryConditionTrait;
  use EudonetQueryFieldTrait;
  use EudonetQueryPagerTrait;
  use EudonetQueryOrderByTrait;

  private $additionalPath;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $http_client_factory, Eudonet $eudonet, EudonetQueryResultManager $eudonet_query_result_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client_factory, $eudonet, $eudonet_query_result_manager);
    $this->additionalPath = $this->configuration['additional_path'] ?? FALSE;
    if (!$this->additionalPath) {
      throw new Exception('A tabId or tabId + fileId  are required to use the search resource');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    $additional_path = explode('/', $this->additionalPath);
    if (in_array('Fast', $additional_path) || count($additional_path) == 1 || in_array('PlanningOccupied', $additional_path)) {
      return 'POST';
    }
    return parent::getMethod();
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return parent::getPath() . $this->additionalPath;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $where_custom = [
      'WhereCustoms' => [],
      'Criteria' => NULL,
      'InterOperator' => Eudonet::INTER_OPERATORS['None'],
    ];
    foreach ($this->conditions as $condition) {
      $where_custom['WhereCustoms'][] = $condition->build();
    }
    $build = [
      'ShowMetaData' => TRUE,
      'ListCols' => $this->fields,
      'WhereCustom' => $where_custom,
    ];
    return array_merge($build, $this->pager, $this->orderBy ?? []);
  }

}
