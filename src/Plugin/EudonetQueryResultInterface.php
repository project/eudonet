<?php

namespace Drupal\eudonet\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Eudonet query result plugins.
 */
interface EudonetQueryResultInterface extends PluginInspectionInterface {

  /**
   * Get success info.
   *
   * @return bool
   *   TRUE if the the request succeed, FALSE otherwise.
   */
  public function success();

  /**
   * Make an auth request if the first request has failed (TOKEN_ERRORS).
   *
   * @return bool|\Drupal\eudonet\Plugin\EudonetQueryResult\AuthenticationQueryResult
   *   TRUE if no need to re-auth, EudonetAuthQueryResult object otherwise.
   */
  public function ensureAuth();

  /**
   * Get the api response message.
   *
   * @return string
   *   The message returned by the api response.
   */
  public function getApiMessage();

  /**
   * Get the error message.
   *
   * @return string
   *   May be empty for a successful request.
   */
  public function getErrorMessage();

  /**
   * Get the error number.
   *
   * @return string
   *   May be empty for a successful request.
   */
  public function getErrorNumber();

  /**
   * Get the raw response object.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   A raw response object from the guzzle request.
   */
  public function getGuzzleResponse();

  /**
   * Get Eudonet API response data.
   *
   * @return array
   *   An array representing the Eudonet API response content.
   */
  public function getResponse();

  /**
   * Header x-call-max => Maximum number of requests allowed.
   *
   * @return int
   *   The maximum number of requests allowed.
   */
  public function getMaxAllowedRequests();

  /**
   * Header x-call-byip-remain => Remaining number of calls for the same ip.
   *
   * @return int
   *   The remaining number of calls for the same ip.
   */
  public function getRemainingCallsByIp();

  /**
   * Header x-call-remain => Number of calls remaining.
   *
   * @return int
   *   The number of calls remaining.
   */
  public function getRemainingCalls();

  /**
   * Test if it is possible to send new request.
   *
   * @return bool
   *   TRUE if it's possible to send a new request, FALSE otherwise.
   */
  public function isExceedQuotas();

}
