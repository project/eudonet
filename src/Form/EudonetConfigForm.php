<?php

namespace Drupal\eudonet\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\eudonet\Eudonet;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EudonetConfigForm.
 */
class EudonetConfigForm extends ConfigFormBase {

  use MessengerTrait;

  /**
   * The Eudonet Helper service.
   *
   * @var \Drupal\eudonet\Eudonet
   */
  protected $eudonet;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, Eudonet $eudonet) {
    parent::__construct($config_factory);
    $this->eudonet = $eudonet;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('eudonet')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->eudonet::EUDONET_CONFIG,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eudonet_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->eudonet::EUDONET_CONFIG);

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API base url'),
      '#description' => $this->t('Exemple: https://xrm.eudonet.com/EudoAPI/'),
      '#default_value' => $config->get('base_url'),
    ];

    $auth = $config->get('authentication');
    $form['authentication'] = [
      '#type' => 'details',
      '#title' => $this->t('Authentication'),
      '#description' => $this->t('Server authentication.'),
      '#open' => (empty($auth)) ? TRUE : FALSE,
      '#tree' => TRUE,
    ];
    $form['authentication']['subscriber_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscriber login'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $auth['subscriber_login'] ?? '',
      '#required' => TRUE,
    ];
    $form['authentication']['subscriber_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Subscriber password'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $auth['subscriber_password'] ?? '',
      '#description' => $this->t('Leave blank to make no changes.'),
    ];
    $form['authentication']['base_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $auth['base_name'] ?? '',
      '#required' => TRUE,
    ];
    $form['authentication']['user_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User login'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $auth['user_login'] ?? '',
      '#required' => TRUE,
    ];
    $form['authentication']['user_password'] = [
      '#type' => 'password',
      '#title' => $this->t('User password'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $auth['user_password'] ?? '',
      '#description' => $this->t('Leave blank to make no changes.'),
    ];
    $form['authentication']['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => [
        'LANG_00' => $this->t('French'),
        'LANG_01' => $this->t('English'),
        'LANG_02' => $this->t('Deutsch'),
        'LANG_03' => $this->t('Nederlands'),
        'LANG_04' => $this->t('Español'),
        'LANG_05' => $this->t('Italiano'),
      ],
      '#multiple' => FALSE,
      '#default_value' => $auth['language'] ?? 'LANG_00',
    ];
    $form['authentication']['product_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $auth['product_name'] ?? '',
    ];

    $token_info = $config->get('token_info');
    $form['token_info'] = [
      '#type' => 'details',
      '#title' => $this->t('Token information'),
      '#open' => (!empty($token_info['token'])) ? TRUE : FALSE,
    ];
    $form['token_info']['expiration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expiration'),
      '#default_value' => $token_info['expiration'] ?? '',
      '#attributes' => [
        'readonly' => '',
        'disabled' => 'disabled',
      ],
    ];
    $form['token_info']['server_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server date'),
      '#default_value' => $token_info['server_date'] ?? '',
      '#attributes' => [
        'readonly' => '',
        'disabled' => 'disabled',
      ],
    ];
    $form['token_info']['token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Current token'),
      '#default_value' => $token_info['token'] ?? '',
      '#attributes' => [
        'readonly' => '',
        'disabled' => 'disabled',
      ],
    ];
    $form['token_info']['actions'] = [
      '#type' => 'actions',
    ];
    $form['token_info']['actions']['try_auth_request'] = [
      '#type' => 'submit',
      '#value' => $this->t('Try auth request'),
      '#submit' => ['::tryAuthenticationRequest'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config($this->eudonet::EUDONET_CONFIG);

    // Manage password elements. For security reasons, we do not want to
    // re-expose the password to the user.
    $old_auth = $config->get('authentication');
    $authentication = $form_state->getValue('authentication');
    if (empty($authentication['subscriber_password'])) {
      $authentication['subscriber_password'] = $old_auth['subscriber_password'];
    }
    if (empty($authentication['user_password'])) {
      $authentication['user_password'] = $old_auth['user_password'];
    }

    $config
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('authentication', $authentication)
      ->save();
  }

  /**
   * Make an authentication request to test the authentication to the API.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function tryAuthenticationRequest(array &$form, FormStateInterface $form_state) {
    try {
      $result = $this->eudonet->authenticate();
      if ($result->success()) {
        $this->messenger()->addStatus($this->t('Successful authentication.'));
      }
      else {
        $this->messenger()->addError($result->getErrorNumber());
        $this->messenger()->addError($result->getErrorMessage());
      }
    }
    catch (GuzzleException $e) {
      $this->messenger()->addError($e->getMessage());
    }
    catch (PluginException $e) {
      $this->messenger()->addError($e->getMessage());
    }
  }

}
