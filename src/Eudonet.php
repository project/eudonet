<?php

namespace Drupal\eudonet;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eudonet\Plugin\EudonetQueryManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Eudonet.
 *
 * @package Drupal\eudonet
 * @see https://xrm.eudonet.com/eudoapi/eudoapidoc/lexique.html
 */
class Eudonet {

  use MessengerTrait;
  use StringTranslationTrait;

  const OPERATORS = [
    '=' => 0,
    '<' => 1,
    '<=' => 2,
    '>' => 3,
    '>=' => 4,
    '!=' => 5,
    '^' => 6,
    '$' => 7,
    'IN' => 8,
    'CONTAINS' => 9,
    'EMPTY' => 10,
    'TRUE' => 11,
    'FALSE' => 12,
    '!^' => 13,
    '!$' => 14,
    'NOT IN' => 15,
    'NOT CONTAINS' => 16,
    'NOT EMPTY' => 17,
  ];

  const INTER_OPERATORS = [
    'None' => 0,
    'AND' => 1,
    'OR' => 2,
    'EXCEPT' => 3,
  ];

  const TOKEN_ERRORS = [100, 101, 102, 103, 104];

  /**
   * The Eudonet query manager used to create query.
   *
   * @var \Drupal\eudonet\Plugin\EudonetQueryManager
   */
  private $eudonetQueryManager;

  /**
   * The config factory service used to the Eudonet global settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * EudonetHelper constructor.
   *
   * @param \Drupal\eudonet\Plugin\EudonetQueryManager $eudonet_query_manager
   *   The Eudonet query manager.
   */
  const EUDONET_CONFIG = 'eudonet.eudonetconfig';

  /**
   * EudonetHelper constructor.
   *
   * @param \Drupal\eudonet\Plugin\EudonetQueryManager $eudonet_query_manager
   *   The Eudonet query manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service use to get the Eudonet global settings.
   */
  public function __construct(EudonetQueryManager $eudonet_query_manager, ConfigFactoryInterface $config_factory) {
    $this->eudonetQueryManager = $eudonet_query_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Use php trim function with custom char list.
   *
   * Include the default + non-breaking space char.
   *
   * @param string $string
   *   The string to trim.
   *
   * @return string
   *   The trimmed string.
   */
  public static function trim($string) {
    return trim($string, "  \t\n\r\0\x0B");
  }

  /**
   * Clean string from CRM. Replace encoded HTML special chars ',",&.
   *
   * @param string $string
   *   The string to clean.
   *
   * @return string
   *   The cleaned string.
   */
  public static function cleanString($string) {
    return static::trim(Xss::filter(str_replace(
      ['&#039;', '&amp;', '&quot;'],
      ["'", "&", '"'],
      $string
    )));
  }

  /**
   * Helper function to get file content and prepare it for upload.
   *
   * The CRM want a base64 encoded file.
   *
   * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
   *   The file object to get and encode.
   *
   * @return string
   *   A string representing the base64 encoded file.
   */
  public static function prepareFileForUpload(UploadedFile $file) {
    return base64_encode($file->openFile()->fread($file->getSize()));
  }

  /**
   * Get the global settings object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   An immutable config object representing the eudonet global settings.
   */
  public function getGlobalSettings() {
    return $this->configFactory->get(self::EUDONET_CONFIG);
  }

  /**
   * Get an editable config object.
   *
   * @return \Drupal\Core\Config\Config
   *   An editable config object representing the Eudonet global settings.
   */
  public function getEditableGlobalSettings() {
    return $this->configFactory->getEditable(self::EUDONET_CONFIG);
  }

  /**
   * Make an auth request.
   *
   * All parameters are optional. The module settings is used instead.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQueryResultInterface
   *   The authentication query result.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function authenticate() {
    return $this->getAuthenticationQuery()->execute();
  }

  /**
   * Shorthand to create an authentication query.
   *
   * @param string $subscriber_login
   *   Optional. You probably should use the BO to settings up this parameter.
   * @param string $subscriber_password
   *   Optional. You probably should use the BO to settings up this parameter.
   * @param string $base_name
   *   Optional. You probably should use the BO to settings up this parameter.
   * @param string $user_login
   *   Optional. You probably should use the BO to settings up this parameter.
   * @param string $user_password
   *   Optional. You probably should use the BO to settings up this parameter.
   * @param string $language
   *   Optional. You probably should use the BO to settings up this parameter.
   * @param string $product_name
   *   Optional. You probably should use the BO to settings up this parameter.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\AuthenticationQuery
   *   An Eudonet query to make authentication request.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getAuthenticationQuery($subscriber_login = '', $subscriber_password = '', $base_name = '', $user_login = '', $user_password = '', $language = '', $product_name = '') {
    $config = $this->getGlobalSettings();
    $auth = $config->get('authentication');

    // Check params.
    $config_params = count(array_filter($auth ?? []));
    $total_config_params = count($auth);
    $func_params = count(func_get_args());
    if (($config_params != $total_config_params && $func_params != 7)) {
      $warn = $this->t('Authentication probably fail due to a missing authentication parameter.');
      $this->messenger()->addWarning($warn);
    }

    /** @var \Drupal\eudonet\Plugin\EudonetQuery\AuthenticationQuery $query */
    $query = $this->eudonetQueryManager->createInstance('eudonet_authentication_query', [
      'authentication_parameters' => [
        'SubscriberLogin' => (!empty($subscriber_login)) ? $subscriber_login : $auth['subscriber_login'],
        'SubscriberPassword' => (!empty($subscriber_password)) ? $subscriber_password : $auth['subscriber_password'],
        'BaseName' => (!empty($base_name)) ? $base_name : $auth['base_name'],
        'UserLogin' => (!empty($user_login)) ? $user_login : $auth['user_login'],
        'UserPassword' => (!empty($user_password)) ? $user_password : $auth['user_password'],
        'UserLang' => (!empty($language)) ? $language : $auth['language'],
        'ProductName' => (!empty($product_name)) ? $product_name : $auth['product_name'],
      ],
    ]);
    return $query;
  }

  /**
   * Get a meta infos query object.
   *
   * @param bool $table_list
   *   Determine if the meta infos query should get all tables or not.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\MetaInfosQuery
   *   A meta infos query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function metaInfos($table_list = TRUE) {
    /** @var \Drupal\eudonet\Plugin\EudonetQuery\MetaInfosQuery $query */
    $query = $this->eudonetQueryManager->createInstance('eudonet_meta_infos_query', [
      'get_table_list' => $table_list,
    ]);
    return $query;
  }

  /**
   * Get a catalog query object.
   *
   * @param int|string $additional_path
   *   The catalog descId or 'Users'.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\CatalogQuery
   *   A catalog query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function catalog($additional_path) {
    /** @var \Drupal\eudonet\Plugin\EudonetQuery\CatalogQuery $query */
    $query = $this->eudonetQueryManager->createInstance('eudonet_catalog_query', [
      'additional_path' => $additional_path,
    ]);
    return $query;
  }

  /**
   * Get a search query object.
   *
   * @param int|string $additional_path
   *   The tabId, tabId/fileId, Fast/tabId or PlanningOccupied.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\SearchQuery
   *   A search query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function search($additional_path) {
    /** @var \Drupal\eudonet\Plugin\EudonetQuery\SearchQuery $query */
    $query = $this->eudonetQueryManager->createInstance('eudonet_search_query', [
      'additional_path' => $additional_path,
    ]);
    return $query;
  }

  /**
   * Get an attachment query object.
   *
   * @param string $file_id
   *   Id of the file on which to add the attachment.
   * @param string $tab_id
   *   DescId of the tab on which the attachment should be added.
   * @param string $filename
   *   Attachment name, if it already exists on the server, it will be named
   *   'name_pj.pj (x)'.
   * @param string $content
   *   Binary stream of the attachment encoded in base 64.
   * @param bool $is_url
   *   Indicates if the attachment is an url.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\AttachmentsQuery
   *   The attachment query object ready to execute.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function attachment($file_id, $tab_id, $filename, $content, $is_url = FALSE) {
    /** @var \Drupal\eudonet\Plugin\EudonetQuery\AttachmentsQuery $query */
    $query = $this->eudonetQueryManager->createInstance('eudonet_attachments_query', [
      'attachment_parameters' => [
        "FileId" => $file_id,
        "TabId" => $tab_id,
        "FileName" => $filename,
        "Content" => $content,
        "IsUrl" => $is_url,
      ],
    ]);
    return $query;
  }

  /**
   * Get a CUD query object to Create/Update/Delete something.
   *
   * @param string $additional_path
   *   The additional path should be a string like:
   *     - {tabId}.
   *     - {tabId}/{fileId}.
   *     - Delete/{tabId}/{fileId}.
   *     - Image/{tabId}/{fileId}/{filename}.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\CUDQuery
   *   A CUD query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function cud($additional_path = '') {
    /** @var \Drupal\eudonet\Plugin\EudonetQuery\CUDQuery $query */
    $query = $this->eudonetQueryManager->createInstance('eudonet_cud_query', [
      'additional_path' => $additional_path,
    ]);
    return $query;
  }

  /**
   * Shorthand for CUD query to create a new file.
   *
   * @param int $tab_id
   *   Id of the table in which to add the new file.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\CUDQuery
   *   A CUD query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function cudCreate($tab_id) {
    return $this->cud($tab_id);
  }

  /**
   * Shorthand for CUD query to update a file id.
   *
   * @param int $tab_id
   *   Id of the table in which the file_id is located.
   * @param int $file_id
   *   The file id to update.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\CUDQuery
   *   A CUD query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function cudUpdate($tab_id, $file_id) {
    return $this->cud("$tab_id/$file_id");
  }

  /**
   * Shorthand for CUD query to update a file's image.
   *
   * @param int $tab_id
   *   Id of the table in which the file_id is located.
   * @param int $file_id
   *   The file id to update.
   * @param string $filename
   *   The filename to update.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\CUDQuery
   *   A CUD query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function cudUpdateImage($tab_id, $file_id, $filename) {
    return $this->cud("Image/$tab_id/$file_id/$filename");
  }

  /**
   * Shorthand for CUD query to delete a file id.
   *
   * @param int $tab_id
   *   Id of the table in which the file_id is located.
   * @param int $file_id
   *   The file id to delete.
   *
   * @return \Drupal\eudonet\Plugin\EudonetQuery\CUDQuery
   *   A CUD query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function cudDelete($tab_id, $file_id) {
    return $this->cud("Delete/$tab_id/$file_id");
  }

}
