<?php

namespace Drupal\eudonet;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for eudonet_mapping managers.
 */
interface EudonetMappingPluginManagerInterface extends PluginManagerInterface {

    // Add getters and other public methods for eudonet_mapping managers.

}
