# Eudonet - Drupal 8 & 9

This module provides a PHP client to make requests to the Eudonet API.

## Available queries

1. Authentication
  * To create a query object => `\Drupal::service('eudonet')->getAuthenticationQuery();`
  * Shorthand to make an authentication request => `\Drupal::service('eudonet')->authenticate();`
2. Catalog
  * `Drupal::service('eudonet')->catalog(267)`
3. Meta infos
  * `Drupal::service('eudonet')->metaInfos(<tablist>)`
4. Search
  * `Drupal::service('eudonet')->search(200)`

## Concepts

2 plugin types are used by this module:

* The EudonetQuery plug-in type is responsible for the http request to the eudonet API.
* The EudonetQueryResult plug-in type is used to abstract (facilitate handling) the response.

### Exemple

```php
<?php
$query = $this->eudonet->search(200);
$query->addFields([
  'last_name',
  'first_name',
  'gender',
]);
$query->condition('gender', 'Masculin');
$query->condition('last_name', NULL, 'NOT EMPTY');
$or = $query->orGroup();
$or->condition('last_name', 'XE', '^');
$or->condition('last_name', 'A.M.B.', '^');
$query->conditionGroup($or);
$result = $query->execute();
/** @var \Drupal\eudonet\EudonetSearchQueryResultItemWrapper $item */
foreach ($result as $item) {
  if (!empty($item->last_name)) {
    $last_name = $item->last_name->Value;
  }
  $file_id = $item->id();
}
```

```php
<?php
$catalog_query = $this->eudonet->catalog(267);
$result = $catalog_query->execute();
foreach ($result as $item) {
  $label = $item['Label'];
  $id = $item['Id'];
}
```
